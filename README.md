# Webpack TypeScript Boilerplate
Prosty projekt implementujący działanie webpack i ts-loader w celu automatycznej kompilacji TypeScript do JavaScript.
##Instalacja
W celu instalacji należy:
1) Pobrać projekt z repozytorium GIT
```git clone https://bitbucket.org/ideschool/webpack-typescript-boilerplate.git```
2) Zainstalować zależne pakiety npm poleceniem
```npm install```
## Działanie
Polecenie ``` npm run dev ``` uruchamia WEbpack w trybie deweloperskim z włączoną opcją Hot Module Replacement.

Polecenie ```npm run build``` buduje cały projekt z ustawieniami produkcyjnymi.
